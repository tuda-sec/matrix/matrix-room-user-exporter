# Matrix Synapse Room and User Stats Exporter

This exporter gather some room and user stats from a synapse homeserver.

## What's currently exported
- Number of users/accounts
  - All users (enabled and disabled)  
    `matrix_synapse_admin_all_accounts`
  - All enabled accounts  
    `matrix_synapse_admin_active_accounts`
- Number of rooms and spaces (currently not distinguishable)  
  - All rooms  
    `matrix_synapse_admin_all_rooms`
  - 1:1 rooms local (rooms with two participants and both are on the own homeserver)  
    `matrix_synapse_admin_one_to_one_rooms_local`
  - 1:1 rooms global (rooms with two participants and only one is on the own homeserver)  
    `matrix_synapse_admin_one_to_one_rooms_global`
  - Group rooms local (rooms with more then two participants and alle are on the own homeserver)  
    `matrix_synapse_admin_groups_local`
  - Group rooms global (rooms with more then two participants and at least one is on the own homeserver)  
    `matrix_synapse_admin_groups_global`
  - Other rooms (only rooms with more then two participants and alle are on the own homeserver)  
    `matrix_synapse_admin_rooms_other`
  - Empty rooms (rooms without participants)  
    `matrix_synapse_admin_rooms_empty`
- Number of users on homeserver (own and otherhomeserver)  
  `matrix_synapse_admin_users_on_homeserver`
- Number of rooms on homeserver (own and other homeserver)  
  `matrix_synapse_admin_rooms_on_homeserver`
- Room size/Number of participants per room (own and other homeserver)  
  `matrix_synapse_admin_rooms_joined_members`
- Room size (local)/Number of participants per room (local) (only own homeserver)  
  `matrix_synapse_admin_rooms_joined_local_members`

## Usage
Set the following enviroment files (e.g. in the docker-compose.yml)  
```
MATRIX_HOMESERVER_URL: https://<URL>
MATRIX_ACCESS_TOKEN: <STRING>
```
OR docker run:
```
docker run -d -e MATRIX_HOMESERVER_URL="https://<URL>" -e MATRIX_ACCESS_TOKEN="<STRING>" -p 9987:9987 --rm registry.gitlab.com/tuda-sec/matrix/matrix-room-user-exporter:main
```


The `MATRIX_HOMESERVER_URL` must point to the synapse main process (not any worker) because worker don't have an Admin API endpoint.   
The `MATRIX_ACCESS_TOKEN` must have admin rights on the homeserver because it uses the Admin API.  
  
! Attention: Depending of the number of rooms and users one run can take several seconds and produces load on the synapse main process. Please set a high `scrape_interval` and `scrape_timeout` in Prometheus (serveral minutes) !
  
This exporter uses Port `9987`
