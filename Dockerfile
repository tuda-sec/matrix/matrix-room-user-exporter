FROM python:3.10-slim

COPY Pipfile Pipfile.lock /

RUN apt-get update \
    && pip install pipenv && pipenv install --system --deploy \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /exporter

COPY main.py ./

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/exporter

EXPOSE 9987

CMD ["python3", "main.py"]