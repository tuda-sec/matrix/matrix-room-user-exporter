import json
import os
import time
from collections import Counter
from ratelimit import limits, sleep_and_retry

import requests
from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY, GaugeMetricFamily

ONE_MINUTE = 60
MAX_CALLS_PER_MINUTE = 240

class MatrixExporter(object):
    matrix_hs = ""
    matrix_at = ""

    def __init__(self):
        self.matrix_hs = os.getenv("MATRIX_HOMESERVER_URL")
        self.matrix_at = os.getenv("MATRIX_ACCESS_TOKEN")
        pass
    
    @sleep_and_retry
    @limits(calls=MAX_CALLS_PER_MINUTE, period=ONE_MINUTE)
    def get_accounts(self, with_deactivated) -> int:
        users_request = requests.get(
            self.matrix_hs
            + "/_synapse/admin/v2/users?from=0&guests=false&limit=0&deactivated="
            + str(with_deactivated).lower(),
            headers={"Authorization": "Bearer " + self.matrix_at},
        )
        return users_request.json()["total"]

    @sleep_and_retry
    @limits(calls=MAX_CALLS_PER_MINUTE, period=ONE_MINUTE)
    def get_rooms(self) -> list:
        rooms_request = requests.get(
            self.matrix_hs + "/_synapse/admin/v1/rooms?limit=10000",
            headers={"Authorization": "Bearer " + self.matrix_at},
        )
        rooms_request_json = rooms_request.json()["rooms"]

        rooms_dict = json.loads(rooms_request.text)
        while "next_batch" in rooms_dict:
            rooms_request = requests.get(
                self.matrix_hs
                + "/_synapse/admin/v1/rooms?limit=10000&from="
                + str(rooms_dict.get("next_batch")),
                headers={"Authorization": "Bearer " + self.matrix_at},
            )
            rooms_request_json.extend(rooms_request.json()["rooms"])
            rooms_dict = json.loads(rooms_request.text)

        return rooms_request_json

    @sleep_and_retry
    @limits(calls=MAX_CALLS_PER_MINUTE, period=ONE_MINUTE)
    def get_users_in_room(self, room_id) -> list:
        room_member_request = requests.get(
            self.matrix_hs + "/_synapse/admin/v1/rooms/" + room_id + "/members",
            headers={"Authorization": "Bearer " + self.matrix_at},
        )
        return room_member_request.json()["members"]

    def collect(self):

        g_all_accounts = GaugeMetricFamily("matrix_synapse_admin_all_accounts", "All accounts (incl deactivated)")
        g_all_accounts.add_metric(["matrix_synapse_admin_all_accounts"], self.get_accounts(True))
        yield g_all_accounts

        g_active_accounts = GaugeMetricFamily("matrix_synapse_admin_active_accounts", "All active accounts")
        g_active_accounts.add_metric(["matrix_synapse_admin_active_accounts"], self.get_accounts(False))
        yield g_active_accounts

        rooms_list = self.get_rooms()

        all_rooms = len(rooms_list)
        g_all_rooms = GaugeMetricFamily("matrix_synapse_admin_all_rooms", "All rooms")
        g_all_rooms.add_metric(["matrix_synapse_admin_all_rooms"], all_rooms)
        yield g_all_rooms

        g_rooms_joined_members = GaugeMetricFamily(
            "matrix_synapse_admin_rooms_joined_members",
            "User in Rooms",
            labels=["room", "name"],
        )
        g_rooms_joined_local_members = GaugeMetricFamily(
            "matrix_synapse_admin_rooms_joined_local_members",
            "Local User in Rooms",
            labels=["room", "name"],
        )
        one_to_one_rooms_local = 0
        one_to_one_rooms_global = 0
        groups_local = 0
        groups_global = 0
        empty_rooms = 0
        rooms_on_homeserver = list()
        room_members = set()

        for room in rooms_list:
            room_id = room["room_id"]
            room_name = room["name"]
            room_joined_members = room["joined_members"]
            room_joined_local_members = room["joined_local_members"]

            room_members.update(self.get_users_in_room(room_id))
            rooms_on_homeserver.append(room_id.split(":")[1])
            room_name = room_name or room_id

            g_rooms_joined_members.add_metric([room_id, room_name], room_joined_members)
            g_rooms_joined_local_members.add_metric([room_id, room_name], room_joined_local_members)

            if room_joined_members == 2 and room_joined_local_members == room_joined_members:
                one_to_one_rooms_local += 1
            if room_joined_members == 2 and room_joined_local_members != room_joined_members:
                one_to_one_rooms_global += 1
            if room_joined_members > 2 and room_joined_local_members == room_joined_members:
                groups_local += 1
            if room_joined_members > 2 and room_joined_local_members != room_joined_members:
                groups_global += 1
            if room_joined_members == 0:
                empty_rooms += 1

        yield g_rooms_joined_members
        yield g_rooms_joined_local_members

        other_rooms = (
            all_rooms - one_to_one_rooms_local - one_to_one_rooms_global - groups_local - groups_global - empty_rooms
        )

        g = GaugeMetricFamily("matrix_synapse_admin_one_to_one_rooms_local", "1:1 Local Chats")
        g.add_metric(["matrix_synapse_admin_one_to_one_rooms_local"], one_to_one_rooms_local)
        yield g

        g = GaugeMetricFamily("matrix_synapse_admin_one_to_one_rooms_global", "1:1 Global Chats")
        g.add_metric(["matrix_synapse_admin_one_to_one_rooms_global"], one_to_one_rooms_global)
        yield g

        g = GaugeMetricFamily("matrix_synapse_admin_groups_local", "Local Group Chats")
        g.add_metric(["matrix_synapse_admin_groups_local"], groups_local)
        yield g

        g = GaugeMetricFamily("matrix_synapse_admin_groups_global", "Global Group Chats")
        g.add_metric(["matrix_synapse_admin_groups_global"], groups_global)
        yield g

        g = GaugeMetricFamily("matrix_synapse_admin_rooms_empty", "Empty Rooms")
        g.add_metric(["matrix_synapse_admin_rooms_empty"], empty_rooms)
        yield g

        g = GaugeMetricFamily("matrix_synapse_admin_rooms_other", "Empty Rooms")
        g.add_metric(["matrix_synapse_admin_rooms_other"], other_rooms)
        yield g

        rooms_on_homeserver = Counter(rooms_on_homeserver).most_common()

        g = GaugeMetricFamily("matrix_synapse_admin_rooms_on_homeserver", "Rooms on Homeserver", labels=["homeserver"])
        for homesever, rooms in rooms_on_homeserver:
            g.add_metric([homesever], rooms)
        yield g

        users_on_homeserver = list()
        for member in room_members:
            users_on_homeserver.append(member.split(":")[1])
        users_on_homeserver = Counter(users_on_homeserver).most_common()
        g = GaugeMetricFamily("matrix_synapse_admin_users_on_homeserver", "Users on Homeserver", labels=["homeserver"])
        for homesever, users in users_on_homeserver:
            g.add_metric([homesever], users)
        yield g


def check_variables():
    if os.getenv("MATRIX_HOMESERVER_URL") is None:
        raise ValueError('Variable "MATRIX_HOMESERVER_URL" not set')
    if os.getenv("MATRIX_ACCESS_TOKEN") is None:
        raise ValueError('Variable "MATRIX_ACCESS_TOKEN" not set')


def main():
    try:
        check_variables()
        start_http_server(9987)
        REGISTRY.register(MatrixExporter())
        while True:
            time.sleep(15)
    except ValueError as e:
        print("An Error occurred")
        print(e)


if __name__ == "__main__":
    main()
